//
//  JLExchangeItTests.swift
//  JLExchangeItTests
//
//  Created by Jhoney Lopes on 24/07/18.
//  Copyright © 2018 Jhoney Lopes. All rights reserved.
//

import XCTest
@testable import JLExchangeIt

extension JLExchangeItApiAssembler {
  func resolveNoResults() -> JLExchangeItModel {
    return JLExchangeItModel(with: "", date: "", currencies: [])
  }
  func resolveUSD() -> JLExchangeItModel {
    return JLExchangeItModel(with: "USD", date: "", currencies: [])
  }
}

class TestJLExchangeItApiAssembler: JLExchangeItApiAssembler { }

class JLExchangeItApiTests: XCTestCase {
  
  override func setUp() {
    super.setUp()
  }
  
  override func tearDown() {
    super.tearDown()
  }
  
  func testNoAddress() {
    getAllCurrencies(with: "", success: { response in
      if let currencies = response as? JLExchangeItModel {
        if currencies.getCurrenciesName().count > 0 {
          XCTAssert(false)
        } else {
          XCTAssert(true)
        }
      }
    }) { error in
      XCTAssert(false)
    }
  }
}

extension JLExchangeItApiTests: JLExchangeItApiProtocol {
  func getAllCurrencies(with base: String, success: @escaping BaseSuccessCallback, failure: @escaping BaseFailureCallback) {
    let testAssembler: JLExchangeItApiAssembler = TestJLExchangeItApiAssembler()
    switch base {
    case "":
      success(testAssembler.resolveNoResults() as AnyObject)
    case "USD":
      success(testAssembler.resolveUSD() as AnyObject)
    default:
      failure(nil)
    }
  }
}
