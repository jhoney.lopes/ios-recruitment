//
//  AppDelegate.swift
//  JLExchangeIt
//
//  Created by Jhoney Lopes on 24/07/18.
//  Copyright © 2018 Jhoney Lopes. All rights reserved.
//

import UIKit

protocol ViewInterface: class {
  func fullScreenLoading(hide: Bool)
}

extension ViewInterface where Self: UIViewController {
  
  func fullScreenLoading(hide: Bool) {
    let offsetPos  = CGFloat(20.0)
    let offsetSize = CGFloat(40.0)
    let tag = 99
    if hide {
      for views in self.view.subviews where views.tag == tag {
        for activity in views.subviews where activity is UIActivityIndicatorView {
          views.removeFromSuperview()
        }
      }
    } else {
      let screenSize = UIScreen.main.bounds
      let screenWidth = screenSize.width
      let screenHeight = screenSize.height
      let view = UIView.init(frame: CGRect.init(x: 0.0, y: 0.0, width: screenWidth, height: screenHeight))
      view.backgroundColor = UIColor.white
      view.tag = tag
      let activityIndicator = UIActivityIndicatorView(frame: CGRect(x: view.center.x-offsetPos,
                                                                    y: view.center.y-offsetPos,
                                                                    width: offsetSize, height: offsetSize))
      activityIndicator.startAnimating()
      activityIndicator.activityIndicatorViewStyle = .whiteLarge
      activityIndicator.color = UIColor.blue
      view.addSubview(activityIndicator)
      self.view.addSubview(view)
    }
  }
}
