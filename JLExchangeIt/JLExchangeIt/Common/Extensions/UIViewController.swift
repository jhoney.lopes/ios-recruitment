//
//  UIViewController.swift
//  JLExchangeIt
//
//  Created by Jhoney Lopes on 24/07/18.
//  Copyright © 2018 Jhoney Lopes. All rights reserved.
//

import UIKit

extension UIViewController {
  
  func hideKeyboardWhenTap() {
    
    let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
    tap.cancelsTouchesInView = false
    view.addGestureRecognizer(tap)
  }
  
  @objc func dismissKeyboard() {
    view.endEditing(true)
  }
}
