//
//  JLExchangeItAPIInteractor.swift
//  JLExchangeIt
//
//  Created by Jhoney Lopes on 24/07/18.
//  Copyright © 2018 Jhoney Lopes. All rights reserved.
//

import Foundation

final class JLExchangeItAPIInteractor: InteractorInterface {
  
  weak var response: JLExchangeItApiInteractorResponseProtocol?
  var provider: JLExchangeItApiProtocol
  
  init(provider: JLExchangeItApiProtocol) {
    self.provider = provider
  }
}

extension JLExchangeItAPIInteractor: JLExchangeItApiInteractorProtocol {
  
  func getAllCurrencies(with base: String) {
    
    guard let resp = response else {
      return
    }
    
    provider.getAllCurrencies(with: base, success: { response in
      if let data = response {
        let model = JLExchangeItModel.parse(with: data)
        resp.responseGetAllCurrenciesSuccess(model)
      }
    }) { error in
      print(error as AnyObject)
      resp.responseGetAllCurrenciesError()
    }
  }
}
