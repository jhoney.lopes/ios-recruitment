//
//  JLExchangeItAPIInteractorProtocol.swift
//  JLExchangeIt
//
//  Created by Jhoney Lopes on 24/07/18.
//  Copyright © 2018 Jhoney Lopes. All rights reserved.
//

import Foundation

protocol JLExchangeItApiInteractorProtocol: InteractorInterface {
  func getAllCurrencies(with base: String)
}

protocol JLExchangeItApiInteractorResponseProtocol: class {
  func responseGetAllCurrenciesSuccess(_ response: JLExchangeItModel)
  func responseGetAllCurrenciesError()
}
