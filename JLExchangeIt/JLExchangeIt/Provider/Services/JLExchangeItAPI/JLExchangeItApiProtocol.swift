//
//  JLExchangeItApiProtocol.swift
//  JLExchangeIt
//
//  Created by Jhoney Lopes on 24/07/18.
//  Copyright © 2018 Jhoney Lopes. All rights reserved.
//

import Foundation

protocol JLExchangeItApiProtocol {
  func getAllCurrencies(with base: String,
                        success: @escaping BaseSuccessCallback,
                        failure: @escaping BaseFailureCallback)
}
