//
//  JLExchangeItApiProvider.swift
//  JLExchangeIt
//
//  Created by Jhoney Lopes on 24/07/18.
//  Copyright © 2018 Jhoney Lopes. All rights reserved.
//

import Foundation

enum JLExchangeItApiProvider {
  
  // MARK: - Cases
  case exchangeIt
  
  // MARK: - Path
  private var path: String {
    switch self {
    case .exchangeIt:
      return "exchangeratesapi.io/api/latest?base=%@"
    }
  }
  
  // MARK: - Methods
  private var method: HTTPMethod {
    switch self {
    case .exchangeIt:
      return .get
    }
  }
  
}

// MARK: - Extensions
extension JLExchangeItApiProvider: JLExchangeItApiProtocol {
  
  func getAllCurrencies(with base: String, success: @escaping BaseSuccessCallback, failure: @escaping BaseFailureCallback) {
    let newPath = path.format(base)
    BaseProvider().request(method: method, endPoint: newPath, params: nil, successBlock: { response in
      success(response)
    }) { error in
      failure(error)
    }
  }

}
