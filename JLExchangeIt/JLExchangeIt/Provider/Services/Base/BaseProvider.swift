//
//  BaseProvider.swift
//  JLExchangeIt
//
//  Created by Jhoney Lopes on 24/07/18.
//  Copyright © 2018 Jhoney Lopes. All rights reserved.
//

import Foundation
import Alamofire

typealias BaseSuccessCallback = (_ data: AnyObject?) -> Swift.Void
typealias BaseFailureCallback = (_ error: Error?) -> Swift.Void

public enum HTTPMethod: String {
  case get    = "GET"
  case put    = "PUT"
  case post   = "POST"
  case delete = "DELETE"
}

internal class BaseProvider {
  
  private enum BaseProviderKeys {
    static let query = "query"
  }
  
  // MARK: Bureau
  fileprivate static let kURL = "http://%@"
  
  // MARK: - Methods
  
  func request(method: HTTPMethod, endPoint: String, params: [String: Any]?, successBlock: @escaping BaseSuccessCallback, failureBlock: @escaping BaseFailureCallback) {
    
    let completeEndPoint = BaseProvider.kURL.format(endPoint)
    let url = completeEndPoint.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
    //let urlString = URL(string: url)
    
    switch method {
    case .get:
      Alamofire.request(url).responseJSON(completionHandler: { response in
        if let value = response.result.value {
          successBlock(value as AnyObject)
        } else {
          guard case let .failure(error) = response.result else { return }
          failureBlock(error)
        }
      })
    case .post:
      NSLog("%@", "No Service POST")
    case .put:
      NSLog("%@", "No Service PUT")
    default:
      NSLog("%@", "Service error")
    }
  }
  
}
