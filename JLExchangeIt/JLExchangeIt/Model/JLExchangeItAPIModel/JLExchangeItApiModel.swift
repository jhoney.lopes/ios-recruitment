//
//  JLExchangeItApiModel.swift
//  JLExchangeIt
//
//  Created by Jhoney Lopes on 24/07/18.
//  Copyright © 2018 Jhoney Lopes. All rights reserved.
//

import Foundation
import SwiftyJSON

struct JLExchangeItModel: BaseResponseProtocol {
  
  // MARK: - Properties
  private var base: String
  private var date: String
  private var currencies: [JLCurrenciesModel]
  
  // MARK: - Init
  init(with base: String, date: String, currencies: [JLCurrenciesModel]) {
    self.base = base
    self.date = date
    self.currencies = currencies
  }
  
  // MARK: - Keys
  private enum Keys {
    static let base  = "base"
    static let date  = "date"
    static let rates = "rates"
  }
  
  // MARK: - Parse
  static func parse(with data: AnyObject) -> JLExchangeItModel {
    let assembler: JLExchangeItModelAssembler = JLExchangeItApiAssembler()
    var exchangeratesapi = assembler.resolve()
    
    if let items = data[Keys.rates] as? [String: Double] {
      let currencies: [String] = Array(items.keys)
      for currencyName in currencies {
        exchangeratesapi.currencies.append(JLCurrenciesModel.parse(with: items, name: currencyName))
      }
    }
    if let base = data[Keys.base] as? String {
      exchangeratesapi.base = base      
    }
    if let date = data[Keys.date] as? String {
      exchangeratesapi.date = date
    }
    
    return exchangeratesapi
  }
  
  // MARK: - Class Funcs
  
  func getBase() -> String {
    return base
  }
  
  func getCurrenciesName() -> [String] {
    var names: [String] = []
    for currency in currencies {
      names.append(currency.getName())
    }
    names.sort()
    return names
  }
  
  func getExchangeValue(with name: String) -> Double {
    let currency = self.currencies.filter{ $0.getName() == name }.first
    return currency?.getValue() ?? 0.00
  }
}

struct JLCurrenciesModel {
  
  // MARK: - Properties
  private var name: String
  private var value: Double
  
  // MARK: - Keys
  private enum JLExchangeItKey {
    static let rates = "rates"
  }
  
  // MARK: - Init
  init(with name: String, value: Double) {
    self.name  = name
    self.value = value
  }
  
  // MARK: - Parse
  static func parse(with dict: [String: Double], name: String) -> JLCurrenciesModel {
    let assembler: JLCurrenciesModelAssembler = JLExchangeItApiAssembler()
    var currency = assembler.resolve()
    
    currency.name  = name
    currency.value = dict[name] ?? 0.00
    
    return currency
  }
  
  // MARK: - Class Funcs
  func getName() -> String {
    return name
  }
  
  func getValue() -> Double {
    return value
  }
}




