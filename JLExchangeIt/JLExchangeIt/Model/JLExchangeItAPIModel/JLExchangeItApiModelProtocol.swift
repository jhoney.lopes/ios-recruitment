//
//  JLExchangeItApiModelProtocol.swift
//  JLExchangeIt
//
//  Created by Jhoney Lopes on 24/07/18.
//  Copyright © 2018 Jhoney Lopes. All rights reserved.
//

import Foundation

protocol JLExchangeItModelAssembler {
  func resolve() -> JLExchangeItModel
}

extension JLExchangeItModelAssembler {
  func resolve() -> JLExchangeItModel {
    return JLExchangeItModel(with: "", date: "", currencies: [])
  }
}

protocol JLCurrenciesModelAssembler {
  func resolve() -> JLCurrenciesModel
}

extension JLCurrenciesModelAssembler {
  func resolve() -> JLCurrenciesModel {
    return JLCurrenciesModel(with: "", value: 0.00)
  }
}

class JLExchangeItApiAssembler: JLExchangeItModelAssembler, JLCurrenciesModelAssembler { }
