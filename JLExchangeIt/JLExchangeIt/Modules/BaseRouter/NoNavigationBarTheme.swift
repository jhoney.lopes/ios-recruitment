//
//  NoNavigationBarTheme.swift
//  JLExchangeIt
//
//  Created by Jhoney Lopes on 24/07/18.
//  Copyright © 2018 Jhoney Lopes. All rights reserved.
//

import UIKit

protocol NoNavigationBarTheme: StatusBarTheme, Themeable { }

extension NoNavigationBarTheme {
  var navigationStatusBarStyle: UIStatusBarStyle? {
    return .default
  }
}

extension NoNavigationBarTheme {
  
  var navigationBarBackgroundColor: UIColor {
    return UIColor.white
  }
  
  var navigationBarTintColor: UIColor {
    return UIColor.clear
  }
  
  var navigationTitleTextAttributes: [NSAttributedStringKey: Any] {
    let font = UIFont.preferredFont(forTextStyle: .headline)
    return [.foregroundColor: UIColor.clear, .font: font]
  }
  
  var navigationBarStyle: UIBarStyle {
    return .default
  }
  
  var navigationBarTranslucent: Bool {
    return true
  }
}
