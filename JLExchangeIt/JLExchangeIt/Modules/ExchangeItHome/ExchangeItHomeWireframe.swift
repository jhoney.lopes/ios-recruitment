//
//  ExchangeItHomeWireframe.swift
//  JLExchangeIt
//
//  Created by Jhoney Lopes on 23/07/18.
//  Copyright (c) 2018 Jhoney Lopes. All rights reserved.
//
//

import UIKit

final class ExchangeItHomeWireframe: BaseWireframe {
  
  // MARK: - Private properties
  private let moduleViewController = ExchangeItHomeViewController(nibName: nil, bundle: nil)
  
  // MARK: - Module setup
  func configureModule(with viewController: ExchangeItHomeViewController) {
    let interactor = JLExchangeItAPIInteractor(provider: JLExchangeItApiProvider.exchangeIt)
    let presenter = ExchangeItHomePresenter(wireframe: self, view: viewController, interactor: interactor)
    viewController.presenter = presenter
    interactor.response = presenter
  }
  
  // MARK: - Transitions
  func show(with transition: Transition, animated: Bool = true) {
    configureModule(with: moduleViewController)
    show(moduleViewController, with: transition, animated: animated)
  }
}

// MARK: - Extensions
extension ExchangeItHomeWireframe: ExchangeItHomeWireframeInterface {
  
  func navigate(to option: ExchangeItHomeNavigationOption) {
//    switch option {
//    case <#pattern#>:
//      <#code#>
//
//    }
  }
}
