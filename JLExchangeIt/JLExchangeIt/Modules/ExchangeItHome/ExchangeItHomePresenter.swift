//
//  ExchangeItHomePresenter.swift
//  JLExchangeIt
//
//  Created by Jhoney Lopes on 23/07/18.
//  Copyright (c) 2018 Jhoney Lopes. All rights reserved.
//
//  This file was generated by the 🐍 VIPER generator
//

import UIKit
import RxSwift
import RxCocoa

final class ExchangeItHomePresenter {
  
  // MARK: - Private properties
  private weak var view: ExchangeItHomeViewInterface?
  private let interactor: JLExchangeItApiInteractorProtocol
  private let wireframe: ExchangeItHomeWireframeInterface
  
  private var model: JLExchangeItModel?
  
  // MARK: - Lifecycle  
  init(wireframe: ExchangeItHomeWireframeInterface,
       view: ExchangeItHomeViewInterface,
       interactor: JLExchangeItApiInteractorProtocol) {
    self.wireframe = wireframe
    self.view = view
    self.interactor = interactor
  }
  
  // MARK: - Class Methods
  func viewDidLoad() {
    self.view?.fullScreenLoading(hide: false)
    interactor.getAllCurrencies(with: ExchangeItHomeConfig.baseCurrency)
  }
}

// MARK: - Extensions
extension ExchangeItHomePresenter: ExchangeItHomePresenterInterface {
  
  func getAllCurrencies(with picker: UIPickerView) {
    _ = picker.rx.itemSelected
      .debounce(0.5, scheduler: MainScheduler.instance)
      .subscribe({ item in
        self.interactor.getAllCurrencies(with: self.getCurrencies()[(item.element?.row) ?? 0])
      })
  }
  
  func getCurrencies() -> [String] {
    if let model = model {
      return model.getCurrenciesName()
    }
    return []
  }
  
  func getBaseCurrency() -> String {
    if let model = model {
      return model.getBase()
    }
    return ""
  }
  
  func getExchangeValue(with name: String) -> Double {
    if let model = model {
      return model.getExchangeValue(with: name)
    }
    return 0.00
  }
  
}

extension ExchangeItHomePresenter: JLExchangeItApiInteractorResponseProtocol {
  func responseGetAllCurrenciesSuccess(_ response: JLExchangeItModel) {
    self.view?.fullScreenLoading(hide: true)
    self.model = response
    self.view?.reloadPicker()
    print(response)
  }
  
  func responseGetAllCurrenciesError() {
    self.view?.fullScreenLoading(hide: true)
  }
}

// MARK: - Enum
enum ExchangeItHomeConfig {
  static let basePicker = 0
  static let baseCurrency = "USD"
  static let zeroValue = "R$ 0.00"
  static let customValue = "R$ %.2f"
}
