//
//  ExchangeItHomeInterfaces.swift
//  JLExchangeIt
//
//  Created by Jhoney Lopes on 23/07/18.
//  Copyright (c) 2018 Jhoney Lopes. All rights reserved.
//
//

import UIKit

enum ExchangeItHomeNavigationOption {
}

protocol ExchangeItHomeWireframeInterface: WireframeInterface {
  func navigate(to option: ExchangeItHomeNavigationOption)
}

protocol ExchangeItHomeViewInterface: ViewInterface {
  func reloadPicker()
}

protocol ExchangeItHomePresenterInterface: PresenterInterface {
  func getAllCurrencies(with picker: UIPickerView)
  func getExchangeValue(with name: String) -> Double
  func getCurrencies() -> [String]
  func getBaseCurrency() -> String
}
