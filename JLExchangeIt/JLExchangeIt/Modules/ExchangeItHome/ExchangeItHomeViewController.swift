//
//  ExchangeItHomeViewController.swift
//  JLExchangeIt
//
//  Created by Jhoney Lopes on 23/07/18.
//  Copyright (c) 2018 Jhoney Lopes. All rights reserved.
//

import UIKit

final class ExchangeItHomeViewController: UIViewController {
  
  // MARK: - Outlets
  @IBOutlet weak var vwHeader: UIView!
  @IBOutlet weak var vwCard: UIView!
  @IBOutlet weak var txfValue: UITextField!
  @IBOutlet weak var lblResult: UILabel!
  @IBOutlet weak var pkvBaseCurrency: UIPickerView!
  @IBOutlet weak var pkvToCurrency: UIPickerView!
  
  // MARK: - Class properties
  override var preferredStatusBarStyle: UIStatusBarStyle {
    let defaultBarStyle: UIStatusBarStyle = .lightContent    
    return defaultBarStyle
  }
  
  // MARK: - Public properties
  var presenter: ExchangeItHomePresenterInterface!
  
  // MARK: - Life Cycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.viewConfiguration()
    presenter.viewDidLoad()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    hideKeyboardWhenTap()
    if let nav = self.navigationController {
      nav.setNavigationBarHidden(true, animated: true)
    }
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    configCard()
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    
  }
  
  // MARK: - Init Deinit
  required convenience init() {
    self.init(nibName: nil, bundle: nil)
  }
  
  deinit { }
  
  // MARK: - Class Configurations
  
  private func viewConfiguration() {
    txfValue.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    txfValue.setBottomBorder()
  }
  
  // MARK: - Class Methods
  private func configCard() {
    vwCard.layer.cornerRadius = 10.0
    vwCard.layer.borderWidth = 1.0
    vwCard.layer.borderColor = UIColor.init(red: 42/255.0, green: 43/255.0, blue: 134/255.0, alpha: 0.36).cgColor
    vwCard.layer.masksToBounds = true            
  }
  
  // MARK: - UIActions
  
}

// MARK: - Extensions
extension ExchangeItHomeViewController: ExchangeItHomeViewInterface {
  
  func reloadPicker() {
    pkvBaseCurrency.reloadAllComponents()
    pkvToCurrency.reloadAllComponents()
    calculateForPicker(currency: presenter.getCurrencies().index(of: presenter.getBaseCurrency()) ?? 0,
                       to: pkvToCurrency.selectedRow(inComponent: 0))
  }
  
  func calculateForPicker(currency: Int, to: Int) {
    pkvBaseCurrency.selectRow(currency, inComponent: 0, animated: true)
    pkvToCurrency.selectRow(to, inComponent: 0, animated: true)
    let exchangeValue = presenter.getExchangeValue(with: presenter.getCurrencies()[to])
    if let currentValue = txfValue.text, let value = Double(currentValue) {
      lblResult.text = String(format: ExchangeItHomeConfig.customValue, value * exchangeValue)
    } else {
      lblResult.text = ExchangeItHomeConfig.zeroValue
    }
  }
  
}

extension ExchangeItHomeViewController: UIPickerViewDelegate, UIPickerViewDataSource {
  func numberOfComponents(in pickerView: UIPickerView) -> Int {
    return 1
  }
  
  func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    let base = ExchangeItHomeConfig.basePicker
    return pickerView.tag == base ? presenter.getCurrencies().count : presenter.getCurrencies().count - 1
  }
  
  func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    return presenter.getCurrencies()[row]
  }
  
  func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    if pickerView.tag == ExchangeItHomeConfig.basePicker {
      presenter.getAllCurrencies(with: pickerView)
    } else {
      calculateForPicker(currency: pkvBaseCurrency.selectedRow(inComponent: 0), to: row)
    }
  }
}

extension ExchangeItHomeViewController: UITextFieldDelegate {
  @objc func textFieldDidChange(_ textField: UITextField) {
    calculateForPicker(currency: pkvBaseCurrency.selectedRow(inComponent: 0), to: pkvToCurrency.selectedRow(inComponent: 0))
  }
}
