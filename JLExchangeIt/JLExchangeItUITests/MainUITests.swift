//
//  MainUITests.swift
//  JLExchangeItUITests
//
//  Created by Jhoney Lopes on 24/07/18.
//  Copyright © 2018 Jhoney Lopes. All rights reserved.
//

import XCTest
import Foundation

class MainUITests: XCTestCase {
  
  // MARK: - Setup
  let app = XCUIApplication()
  let elementQuery = XCUIApplication().scrollViews.otherElements
  
  override func setUp() {
    super.setUp()
    continueAfterFailure = false
    XCUIApplication().launch()
  }
  
  override func tearDown() {
    super.tearDown()
  }
  
  // MARK: - Commons
  func waitElement(element: Any, timeout: TimeInterval = 100.0) {
    let exists = NSPredicate(format: "exists == 1")
    expectation(for: exists, evaluatedWith: element, handler: nil)
    waitForExpectations(timeout: timeout, handler: nil)
  }
}
